execute 'apt-get update'

execute 'apt-get dist-upgrade -y'

%w(
  build-essential
  runit
  ntp
  mercurial
  zip
).each do |p|
  package p
end

include_recipe 'runit::default'
